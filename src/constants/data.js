const car_data = [
    {
      id: 1,
      title: 'Cars',
      routeName: 'cars',
      linkUrl: 'cars',
      size: 'normal',
      imageUrl: 'https://onlinehyderabad.in/wp-content/uploads/2019/06/luxury-car-showroom-748x410.jpg',
      marcas: [
          {
              id: 1,
              title: 'Mercedes Benz',
              imageUrl: 'https://piks-eldesmarqueporta.netdna-ssl.com/thumbs/680/bin/2019/10/26/mercedes.jpg',
              linkUrl: 'mercedesbenz',
              items: [
                {
                  id: 1,
                  name: 'AMG GT',
                  imageUrl: 'https://cdn.autobild.es/sites/navi.axelspringer.es/public/media/image/2018/03/mercedes-amg-gt-r-domanig-autodesign.jpg',
                  price: 25
                },
                {
                  id: 2,
                  name: 'SL Coupe',
                  imageUrl: 'https://images.hgmsites.net/hug/2020-mercedes-benz-sl-class_100720723_h.jpg',
                  price: 18
                }
              ]
          },
          {
            id: 2,
            title: 'BMW',
            linkUrl: 'bmw',
            imageUrl: 'https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/bmw-2020-logo-1583420702.png?crop=0.891xw:0.770xh;0.0523xw,0.109xh&resize=480:*',
            items: [
              {
                id: 1,
                name: 'Z4',
                imageUrl: 'https://motor.elpais.com/wp-content/uploads/2018/08/07a4aabf-2019-bmw-z4-44.jpg',
                price: 25
              },
              {
                id: 2,
                name: 'M8',
                imageUrl: 'https://www.diariomotor.com/imagenes/picscache/750x/bmw-m8-2020-0619-054_750x.jpg',
                price: 18
              },
              {
                id: 3,
                name: 'M4',
                imageUrl: 'https://cdn.motor1.com/images/mgl/ZpA33/s1/2021-bmw-m4-coupe-rendering-by-motor1.com.jpg',
                price: 18
              },
              {
                id: 4,
                name: 'M4',
                imageUrl: 'https://cdn.motor1.com/images/mgl/nRrGR/s3/bmw-z8-roadster.jpg',
                price: 18
              },
              {
                id: 4,
                name: 'M4',
                imageUrl: 'https://images1.autocasion.com/unsafe/1200x800/actualidad/wp-content/uploads/2020/01/1993-BMW-Alpina-B12-5.jpg',
                price: 18
              }

            ]
        },
        {
            id: 3,
            title: 'Porsche',
            linkUrl: 'porsche',
            imageUrl: 'https://1000marcas.net/wp-content/uploads/2019/12/Porsche-logotipo.jpg',
            items: [
              {
                id: 1,
                name: 'GT4',
                imageUrl: 'https://www.alastairbols.com/wp-content/uploads/2019/06/Porsche-Cayman-GT4-Clubsport-for-sale-in-Guards-Red-24-of-30.jpg',
                price: 25
              },
              {
                id: 2,
                name: 'GT3',
                imageUrl: 'https://presskit.porsche.de/models/assets/images/e/29R0138-1add718e.jpg',
                price: 25
              },
              {
                id: 3,
                name: 'GT2',
                imageUrl: 'https://cdn.motor1.com/images/mgl/GE718/s1/2018-porsche-911-gt2-rs-first-drive.jpg',
                price: 25
              },
              {
                id: 4,
                name: '911 Carrera Turbo S',
                imageUrl: 'https://lh3.googleusercontent.com/proxy/ivE26IQH95mcTSfM3JsknrfLrgB88PU0HqXfyyzdyPb5U-IiNpBhy7SE13XJVCkrHc3WJypFqj2FKd1aHjwqlumk0ARJjs-IHEOUwEKB2tyT_VN1NYhudEMs7_yR5w_DbdwuT53LMwXMVw',
                price: 25
              },
              {
                id: 5,
                name: '911 Carrera Turbo S Cabriolet',
                imageUrl: 'https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/2020-porsche-911-carrera-s-cabriolet-120-1583936334.jpg',
                price: 25
              },
              {
                id: 6,
                name: 'Moby Dyck',
                imageUrl: 'https://cdn.motor1.com/images/mgl/k2mOR/s1/porsche-935.jpg',
                price: 25
              },
              
            ]
        },
        {
            id: 4,
            title: 'Audi',
            imageUrl: 'https://i.pinimg.com/originals/75/36/85/75368501cb5135f0605443b0b752c719.jpg',
            linkUrl: 'audi',
            items: [
              {
                id: 1,
                name: 'TT',
                imageUrl: 'https://3.bp.blogspot.com/-g1zTXnOv5Ts/VXt7JQOVlmI/AAAAAAABctA/IDmDNoIuC9k/s1600/Audi-TT-Coup%25C3%25A9-Uruguay%2B%252811%2529.jpg',
                price: 25
              },
              {
                id: 2,
                name: 'R8',
                imageUrl: 'https://www.motoryracing.com/images/noticias/33000/33500/1.jpg',
                price: 18
              },
              {
                id: 3,
                name: 'E Tron',
                imageUrl: 'https://k.uecdn.es/html5/html5lib/v2.73.3_ue_1/modules/KalturaSupport/thumbnail.php/p/110/uiconf_id/14969339/entry_id/0_glczy3of/height/407?',
                price: 18
              }
            ]
        },
        {
            id: 4,
            title: 'Ferrari',
            imageUrl: 'https://i.pinimg.com/originals/db/c2/a4/dbc2a49ebe7034563c6d8331f139f2b5.jpg',
            linkUrl: 'ferrari',
            items: [
              {
                id: 1,
                name: '12 Berlinetta',
                imageUrl: 'https://tictv.com.ve/wp-content/uploads/2020/04/Principal_Ferrari.jpg',
                price: 25
              },
              {
                id: 1,
                name: '812 GTS',
                imageUrl: 'https://www.diariomotor.com/imagenes/2021/02/novitec-ferrari-812-gts-2.jpg',
                price: 25
              },
              {
                id: 2,
                name: '458',
                imageUrl: 'https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/ferrari-458-speciale-blindado-addarmor-1608306513.jpg',
                price: 18
              },
              {
                id: 2,
                name: 'F8 Tributo',
                imageUrl: 'https://www.motor.es/fotos-noticias/2019/03/ferrari-f8-tributo-ginebra-2019-201955343_1.jpg',
                price: 18
              },
              {
                id: 2,
                name: 'Monsa SP1',
                imageUrl: 'https://www.classicdriver.com/sites/default/files/cars_images/ferrari_monza_sp1_sp2_1_2.jpg',
                price: 18
              },
              {
                id: 2,
                name: 'SF90',
                imageUrl: 'https://i.pinimg.com/originals/23/ac/eb/23acebe455e69580137f07a8eff534d5.jpg',
                price: 18
              }
            ]
        }
      ]
      
    },
    {
      id: 2,
      title: 'Bikes',
      routeName: 'bikes',
      linkUrl: 'bikes',
      imageUrl: 'https://i.ytimg.com/vi/q817RdCLc-0/maxresdefault.jpg',
      marcas: [
        {
            id: 1,
            title: 'Honda',
            linkUrl: 'honda',
            imageUrl: 'https://www.expoexhibitionstands.com/wp-content/uploads/2019/06/Honda-Motorcycles-2.jpg',
            items: [
              {
                id: 1,
                name: 'CBR100RR',
                imageUrl: 'https://i.blogs.es/665d8e/honda-cbr1000rr-r-sp-fireblade-2020-014/1366_2000.jpg',
                price: 25
              },
              {
                id: 2,
                name: 'CB1000r',
                imageUrl: 'https://www.motofichas.com/images/phocagallery/Honda/cb1000r-2021/01-honda-cb1000r-2021-estudio-negro.jpg',
                price: 18
              },
              {
                id: 3,
                name: 'CBr650r',
                imageUrl: 'https://www.moto1pro.com/sites/default/files/cbr_red.jpg',
                price: 18
              },
              {
                id: 4,
                name: 'African Twin',
                imageUrl: 'https://www.publimotos.com/images/2020/agosto/18-agosto/WhatsApp%20Image%202020-08-18%20at%202.46.51%20PM.jpeg',
                price: 18
              },
              {
                id: 5,
                name: 'RC213V-S',
                imageUrl: 'https://s3.eu-west-1.amazonaws.com/cdn.motorbikemag.es/wp-content/uploads/2015/08/Honda-RC-213-VS-2016-001-530x380.jpg',
                price: 18
              }
            ]
        },
        {
            id: 2,
            title: 'Triumph',
            linkUrl: 'triumph',
            imageUrl: 'https://previews.123rf.com/images/tofudevil/tofudevil1701/tofudevil170100117/71431731-nonthaburi-november-30-triumph-motorcycle-on-display-at-thailand-international-motor-expo-2016-on-de.jpg',
            items: [
              {
                id: 1,
                name: 'Rocket III Black Edition',
                imageUrl: 'https://www.motor.com.co/files/article_multimedia/uploads/2021/03/16/6050de5280315.jpeg',
                price: 25
              },
              {
                id: 2,
                name: 'Rocket III R',
                imageUrl: 'https://www.soymotero.net/sites/default/files/styles/max_width_800px/public/2019-11/triumph_rocket_3_r_2020_10.jpg?itok=BUV9LQsJ',
                price: 25
              },
              {
                id: 3,
                name: 'Bonneville Boober',
                imageUrl: 'https://2yrh403fk8vd1hz9ro2n46dd-wpengine.netdna-ssl.com/wp-content/uploads/2021/02/2022-Triumph-Bonneville-Bobber-First-Look-retro-classic-sport-urban-motorcycle-15.jpg',
                price: 18
              }
            ]
        },
        {
            id: 3,
            title: 'Ducati',
            linkUrl: 'ducati',
            imageUrl: 'https://i.pinimg.com/originals/3f/d0/67/3fd067ca03faeba92c965c6cafb80836.jpg',
            items: [
              {
                id: 1,
                name: 'Panigale V4',
                imageUrl: 'https://i.blogs.es/e997b7/ducati-panigale-v4-r-2019-005/840_560.jpg',
                price: 25
              },
              {
                id: 2,
                name: 'Street Fighter V4',
                imageUrl: 'https://www.motofichas.com/images/cache/01-ducati-streefighter-v4-s-2020-estudio-rojo-739-a.jpg',
                price: 18
              },
              {
                id: 2,
                name: 'Diabel 1260',
                imageUrl: 'https://www.soymotero.net/sites/default/files/styles/max_width_800px/public/2020-11/my20_ducati_diavel_1260_s_0.jpg?itok=s9Qz0UpD',
                price: 18
              }
              
            ]
        },
        {
            id: 3,
            title: 'BMW',
            linkUrl: 'bmw',
            imageUrl: 'https://www.expomobilia.com/-/media/expomobilia/Images/Content/Messebau/Industrie-Services/BMW-Motorrad/bmw-motorrad-003-swiss-moto-2017-zurich-expomobilia.jpg?la=en-US&mw=1100',
            items: [
              {
                id: 1,
                name: 'HP4',
                imageUrl: 'https://s3.eu-west-1.amazonaws.com/cdn.motorbikemag.es/wp-content/uploads/2017/04/BMW-HP4-RACE-2018-41-1100x470.jpg',
                price: 25
              },
              {
                id: 2,
                name: 'S1000RR',
                imageUrl: 'https://i.blogs.es/dc72b3/bmw-m-1000-rr-2021-003/1366_2000.jpg',
                price: 18
              },
              {
                id: 3,
                name: 'Nine T Racer',
                imageUrl: 'https://www.motofichas.com/images/phocagallery/BMW_Motorrad/R_nineT_Racer_2017/01-BMW-R-nineT-racer-2017-estudio-bmw-motorsport.jpg',
                price: 18
              },
              {
                id: 4,
                name: 'R18',
                imageUrl: 'https://www.revistadelmotor.es/wp-content/uploads/2020/04/P90386371-highRes.jpg',
                price: 18
              },
              {
                id: 5,
                name: 'R 1250 R',
                imageUrl: 'https://i.pinimg.com/1200x/25/ac/14/25ac14d9532392776e24dddc3eaa8559.jpg',
                price: 18
              }
            ]
        }
    ]
    }
  ];

export default car_data;