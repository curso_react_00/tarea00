import React from 'react';
import { Switch, Route} from 'react-router-dom';
import Header from './components/header/header';
import Footer from './components/footer/footer';
import HomePage from './pages/homepage/homepage';
import SubCategory from './components/subcategory/subcategory';

class App extends React.Component {
  constructor() {
    super();

    this.state = {
      currentUser : null,
    };
  }
    componentDidMount() {
      console.log("componentDidMount");
    }

    componentWillUnmount() {
      console.log("componentWillUnmount");
    }

    render() {
      return (
        <div>
          <Header currentUser={this.state.currentUser} />
          <Switch>
            <Route exact path='/' component={HomePage} />
            <Route path='/category/bikes' component={SubCategory} />
            <Route path='/category/cars' component={SubCategory} />
          </Switch>
          <Footer />
        </div>
      );
    }
}

export default App;

/*
<Route exact path='/category/bikes/' component={SubCategory} />
<Route exact path='/category/cars/' component={SubCategory} />
<Route path='/category/bikes/*' component={Item} />
<Route path='/category/cars/*' component={Item} />
*/