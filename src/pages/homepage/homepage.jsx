import React from 'react';
import './homepage.scss'
import Category from '../../components/category/category';

const HomePage = () => (
    <div className="homepage">
        <Category HomePage />
    </div>
);
export default HomePage;