import React from 'react';
import { withRouter } from 'react-router-dom'
import Caradata from '../../constants/data';
import './menu-item.scss'

//const MenuItem = ({ otherSectionProps, history, match }) => (
const MenuItem = ({ tipo,title, imageUrl, size, linkUrl, routeName, history, match }) => {
    const categoria = window.location.pathname.split('/')[2]
    const categoria3 = window.location.pathname.split('/')[3]
    console.log(categoria+"--"+categoria3)
    const direccion = tipo === 1 ?(
        'category'
    ):(
        ``
    )
    return (

        tipo <= 2 ? (

            <div 
                className={`${size} menu-item`}
                onClick={() => history.push(`${match.url}${direccion}/${linkUrl}`)}
            >   
                <div
                    className="background-image"
                    style={{
                        backgroundImage: `url(${imageUrl})`
                    }}
                />
                <div className='content'>
                    <h1 className='title'>{title.toUpperCase()}</h1>
                </div>
            </div>
        ) : (

            Caradata.map( ( item, id ) => {
                if(categoria === item.routeName) {
                    console.log(categoria)
                    return(
                        item.marcas.map( ( item2, id2) => {
                            if(categoria3 === item2.linkUrl){
                                console.log(categoria3)
                                return(
                                    item2.items.map( ( item3, ...otherSectionProps) => {
                                        {console.log(item3.name)}
                                        return(
                                            <div 
                                                className={` menu-item`}
                                            >   
                                                <div
                                                    className="background-image"
                                                    style={{
                                                        backgroundImage: `url(${item3.imageUrl})`
                                                    }}
                                                />
                                                <div className='content'>
                                                    <h1 className='title'>{item3.name}</h1>
                                                </div>
                                            </div>
                                        )
                                    })
                                )
                            }
                        })
                    )
                }
            } )
        ))
    
};

export default withRouter ( MenuItem );