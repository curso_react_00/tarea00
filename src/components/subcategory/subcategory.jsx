import React from 'react';
import Caradata from '../../constants/data';
import MenuItem from '../menu-item/menu-item';

import "./subcategory.scss"

const SubCategory = ( children ) => {
    const categoria = window.location.pathname.split('/')[2]
    const categoria3 = window.location.pathname.split('/')[3]
    const tipo = categoria3 ? 3 : 2;
        return (

            tipo <= 2 ? (
                <div className='directory-menu'>
                    {
                        Caradata.map( ( item, id1, ...otherSectionProps ) => {
                            if(categoria === item.routeName)
                                return (
                                    item.marcas.map( ( {item2, id2, ...otherSectionProps}) => (
                                                <MenuItem key={id2} tipo={tipo} {...otherSectionProps} />
                                    ))
                                )
                            } )
                    }
                </div>
            ):(
                <div className='directory-menu'>
                    {
                        <MenuItem tipo={tipo} />
                    }
                </div>
            )
        )
}

export default SubCategory;