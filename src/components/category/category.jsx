import React from 'react';
import Caradata from '../../constants/data';
import MenuItem from '../menu-item/menu-item';

import "./category.scss"

class Category extends React.Component {
    constructor() {
        super();
        this.state = {
            tipo: 1,
        }
    }

    render() {
        return (
            <div className='directory-menu'>
                {
                    Caradata.map( ({id, ...otherSectionProps}) => (
                        <MenuItem key={id} tipo={this.state.tipo} {...otherSectionProps} />
                    ) )

                }
            </div>
        )
    }
}

export default Category;