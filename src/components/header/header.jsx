import React from 'react';
import { Link } from 'react-router-dom';

const Header = ({ currentUser }) => (
    <div className='header' >
        <div className='options'>
            <h1><Link className='option' to='/' >CAR APP</Link></h1>
        </div>
    </div>
);

export default Header;