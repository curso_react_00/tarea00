import React from 'react';

const Footer = () => (
    <footer>
        <p>
            &copy; { new Date().getFullYear() }- Danilo Moya
        </p>
    </footer>
);

export default Footer;